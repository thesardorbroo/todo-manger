package todo.manager.todo;

public interface TodoList {
    void authenticate();

    void deleteAll();
}
