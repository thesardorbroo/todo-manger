package todo.manager.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import todo.manager.domain.Customer;

/**
 * Spring Data JPA repository for the Customer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT c FROM Customer c WHERE c.user.login = :login")
    Optional<Customer> getByUserLogin(@Param("login") String login);

    Optional<List<Customer>> findAllByGroupId(Long groupId);

    void deleteByGroupId(Long id);
}
